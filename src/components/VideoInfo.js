import React, { Component, useEffect, useState } from 'react';
import axios from 'axios';
import { Fade } from 'reactstrap';
import { Redirect } from 'react-router'

const VideoInfo = () => {
    const [redirect, setRedirect] = useState(false);
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [redirectUserAction, setRedirectUserAction] = useState(false);
    const [initialData, setInitialData] = useState(null);
    const [videos, setVideos] = useState([]);
    const attempts = 8;
    let attempt = 0;
    let videoIndex = Math.floor(Math.random() * (4 - 0 + 1)) + 0;

    useEffect(() => {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        let API_URL = localStorage.getItem('API_URL');
        let MACHINE_CODE = localStorage.getItem('MACHINE_CODE');
        let API_TOKEN = localStorage.getItem('API_TOKEN');

        const getVideo = async () => {
            try {
                let result = await axios.post(`${API_URL}/promotion/videos`, { code: MACHINE_CODE, cancelToken: source.token }, {
                    headers: { 
                        'x-api-key': `${API_TOKEN}`,
                        'Content-Type': 'application/json',
                        'cache-control': 'no-cache'
                    }
                });
                setVideos(result.data.result);
            } catch (error) {
                if (axios.isCancel(error)) {
                    console.log("axios cancelled");
                  } else {
                        if (error.response) {
                            setError("Ha ocurrido un error en el servidor");
                        } else if (error.request) {
                            setError("Verifique la conexión a internet");
                        } else {
                            setError("Error al cargar datos");
                        }
                    }
            }
        }
        getVideo();
    }, [])


    useEffect(() => {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        const getData = async () => {
            try {
                let result = await axios.get(`sensors/cont.txt`, { cancelToken: source.token });
                setData(result.data);
            } catch (error) {
                if (axios.isCancel(error)) {
                    console.log("axios cancelled");
                  } else {
                        if (error.response) {
                            setError("Ha ocurrido un error en el servidor");
                        } else if (error.request) {
                            setError("Verifique la conexión a internet");
                        } else {
                            setError("Error al cargar datos");
                        }
                    }
            }
        }

        if (initialData == null) {
            getData();
            setInitialData(data);
        }
        
        const intervalId = setInterval(async () => {
            if(data != initialData) {
                clearInterval(intervalId);
                setRedirectUserAction(true);
            }

            if (attempt++ == attempts) {
                clearInterval(intervalId);
                setRedirect(true);
            }
            await getData();
        }, 2000);

        return () => {
            source.cancel();
            clearInterval(intervalId);
        }
    }, [data, initialData, attempt]);

    if (redirectUserAction) {
        localStorage.setItem('initialData', initialData);
        return (
            <Redirect to={
                { pathname: '/UserInput' }
            } />
        ) 
    }

    if (redirect) {
        return (
            <Redirect to={"/"} initialData={initialData} />
        )
    }
        return (
            <Fade>
                {
                    videos && videos.length > 0 &&
                    <video id="background-video" loop autoPlay muted>
                        <source src={videos[videoIndex]} type="video/mp4" />
                        Your browser does not support the video tag.
                    </video>
                }
            </Fade>
        );
    
}

export default VideoInfo;