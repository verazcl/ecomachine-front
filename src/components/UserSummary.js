import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Container, Row, Col, Fade, Card, CardImg, CardBody, CardTitle } from 'reactstrap';
import bottleContainer from '../images/icons8-wine-bottle-100.png';
import canContainer from '../images/icons8-beer-can-100.png';
import capContainer from '../images/icons8-bottle-cap-100.png';
import batteryContainer from '../images/icons8-battery-100.png';
import phoneContainer from '../images/icons8-iphone-x-64.png';
import { Redirect } from 'react-router'
import NumberFormat from 'react-number-format';
import QRCode from 'qrcode.react';
import Grid from '@material-ui/core/Grid';
import { v4 as uuidv4 } from 'uuid'

const UserSummary = props => {
    const [redirect, setRedirect] = useState(false);
    const [data, setData] = useState(null);
    const [dataArray, setDataArray] = useState([0, 0, 0, 0, 0, 0, 0]);
    const [error, setError] = useState(null);
    const [totalItems, setTotalItems] = useState(0);
    const [dataCoded, setDataCoded] = useState(null);
    const maxIdle = 20;
    let attemptIdle = 0;
    const processCode = uuidv4();
    let API_URL = localStorage.getItem('API_URL');
    let MACHINE_CODE = localStorage.getItem('MACHINE_CODE');
    let API_TOKEN = localStorage.getItem('API_TOKEN');
    //const currentDate = Date.now();

    if (data == null) {
        setData(localStorage.getItem('finalData'));
        setDataCoded(new Buffer(localStorage.getItem('finalData')).toString('base64'));
    }

    if (totalItems == 0) {
        setTotalItems(localStorage.getItem('totalData'));
    }

    useEffect(() => {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        
        if (data != null) {
            setDataArray(data.split(','));
        }

        const sendData = async () => {
            try {
                
                let result = await axios.post(`${API_URL}/process/save`, { code: MACHINE_CODE, entries: dataCoded, processCode: processCode, cancelToken: source.token }, {
                    headers: { 
                        'x-api-key': `${API_TOKEN}`,
                        'Content-Type': 'application/json',
                        'cache-control': 'no-cache'
                    }
                });
            } catch (error) {
                if (axios.isCancel(error)) {
                    console.log("axios cancelled");
                  } else {
                        if (error.response) {
                            setError("Ha ocurrido un error en el servidor");
                        } else if (error.request) {
                            setError("Verifique la conexión a internet");
                        } else {
                            setError("Error al cargar datos");
                        }
                    }
            }
        }

        if (data != null) {
            sendData();
        }

        const timer = setTimeout(() => {
            setRedirect(true);
        }, 10000);
        
        return () => {
            localStorage.setItem('finalData', null);
            localStorage.setItem('totalData', null);
            localStorage.setItem('initialData', null);
            clearTimeout(timer);
            source.cancel();
        }
    }, []);

    if (redirect) {
        return (
            <Redirect to={"/"} />
        )
    }
        return (
            <Fade>
                <div>
                    <Container fluid>
                        <Row style={{ marginTop: 50 }}>
                            <Col>
                                <h1 className="display-4">
                                    <NumberFormat value={totalItems} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                </h1>
                                <p className="h3 display-4" style={{ color: 'grey', fontSize: '1.9em' }}>Residuos ingresados</p>
                            </Col>
                        </Row>
                        <Row style={{ marginTop: 50 }}>
                            <Col>
                                <Card style={{ alignItems: 'center', borderWidth: 0 }}>
                                    <CardImg top src={bottleContainer} style={{ width: 100 }} />
                                    <CardBody className="mt-0 pt-2">
                                        <CardTitle className="mb-0 mt-0">
                                            <p className="h3 display-4" style={{ color: 'grey', fontSize: '1.9em', fontWeight: '400' }}>
                                                <NumberFormat value={dataArray[0]} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                            </p>
                                        </CardTitle>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col>
                                <Card style={{ alignItems: 'center', borderWidth: 0 }}>
                                    <CardImg top src={canContainer} style={{ width: 100 }} />
                                    <CardBody className="mt-0 pt-2">
                                        <CardTitle className="mb-0 mt-0">
                                            <p className="h3 display-4" style={{ color: 'grey', fontSize: '1.9em', fontWeight: '400' }}>
                                                <NumberFormat value={dataArray[1]} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                            </p>
                                        </CardTitle>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col>
                                <Card style={{ alignItems: 'center', borderWidth: 0 }}>
                                    <CardImg top src={capContainer} style={{ width: 100 }} />
                                    <CardTitle className="mb-0 mt-0">
                                        <p className="h3 display-4" style={{ color: 'grey', fontSize: '1.9em', fontWeight: '400' }}>
                                            <NumberFormat value={dataArray[2]} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                        </p>
                                    </CardTitle>
                                </Card>
                            </Col>
                            <Col>
                                <Card style={{ alignItems: 'center', borderWidth: 0 }}>
                                    <CardImg top src={batteryContainer} style={{ width: 100 }} />
                                    <CardTitle className="mb-0 mt-0">
                                        <p className="h3 display-4" style={{ color: 'grey', fontSize: '1.9em', fontWeight: '400' }}>
                                            <NumberFormat value={dataArray[3]} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                        </p>
                                    </CardTitle>
                                </Card>
                            </Col>
                            <Col>
                                <Card style={{ alignItems: 'center', borderWidth: 0 }}>
                                    <CardImg top src={phoneContainer} style={{ width: 100 }} />
                                    <CardTitle className="mb-0 mt-0">
                                        <p className="h3 display-4" style={{ color: 'grey', fontSize: '1.9em', fontWeight: '400' }}>
                                            <NumberFormat value={dataArray[4]} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                        </p>
                                    </CardTitle>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                    <Grid container spacing={3} alignContent='center'>
                        <Grid item xs={8}>
                            <p className="h3 display-4" style={{ color: 'grey', fontSize: '1.9em' }}>Escanea este código QR y acumula puntos por reciclar!</p>
                        </Grid>
                        <Grid item xs={4}>
                            {  
                                data && dataCoded &&
                                <QRCode value={`${MACHINE_CODE}/${processCode}/${dataCoded}`} />
                            }
                        </Grid>
                    </Grid>
                </div>
            </Fade>
        );
}

export default UserSummary;