import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Container, Row, Col, Fade, Card, CardImg, CardBody, CardTitle, Button } from 'reactstrap';
import bottleContainer from '../images/icons8-wine-bottle-100.png';
import canContainer from '../images/icons8-beer-can-100.png';
import capContainer from '../images/icons8-bottle-cap-100.png';
import batteryContainer from '../images/icons8-battery-100.png';
import phoneContainer from '../images/icons8-iphone-x-64.png';
import { Redirect } from 'react-router'
import NumberFormat from 'react-number-format';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';

const UserInput = props => {
    const [redirect, setRedirect] = useState(false);
    const [data, setData] = useState(null);
    const [initialSensorData, setInitialSensorData] = useState(null);
    const [dataArray, setDataArray] = useState([0, 0, 0, 0, 0, 0, 0]);
    const [error, setError] = useState(null);
    const [initialData, setInitialData] = useState(null);
    const [openDialog, setOpenDialog] = useState(false);
    const [totalItems, setTotalItems] = useState(0);
    const maxIdle = 20;
    let attemptIdle = 0;

    useEffect(() => {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        
        if (initialData == null) {
            console.log('localStorage', localStorage.getItem('initialData'));
            setInitialData(localStorage.getItem('initialData'));
        }

        const getData = async () => {
            try {
                let result = await axios.get(`sensors/cont.txt`, { cancelToken: source.token });
                console.log(result);
                setData(result.data);
            } catch (error) {
                if (axios.isCancel(error)) {
                    console.log("axios cancelled");
                  } else {
                        if (error.response) {
                            setError("Ha ocurrido un error en el servidor");
                        } else if (error.request) {
                            setError("Verifique la conexión a internet");
                        } else {
                            setError("Error al cargar datos");
                        }
                    }
            }
        }
        getData();
        try {
            const intervalId = setInterval(async () => {
                await getData();
                let dataArray = data.split(";");
                let total = 0;
                for (let i in dataArray) {
                    dataArray[i] = dataArray[i] - (initialData.split(";"))[i];
                    total += parseInt(dataArray[i]);
                }
                setTotalItems(total);
                localStorage.setItem('totalData', total);
                
                if (initialSensorData != data) {
                    setInitialSensorData(data);
                    localStorage.setItem('finalData', dataArray);
                    attemptIdle = 0;
                } else {
                    attemptIdle++;
                }
                
                setDataArray(dataArray);

                if (attemptIdle >= maxIdle) {
                    console.log('se alcanzó el tiempo de espera, se terminará el proceso');
                    setOpenDialog(true);
                }

                if (redirect) {
                    clearInterval(intervalId);
                }   
            }, 2000);

            return () => {
                source.cancel();
                clearInterval(intervalId);
            }
        } catch (e) {
            console.log(e);
        }
    }, [data, attemptIdle, redirect, initialSensorData]);

    const handleClickFinish = () => {
        setOpenDialog(true);
    }

    const handleOK = () => {
        setRedirect(true);
      };
    
      const handleCancel = () => {
        attemptIdle = 0;
        setOpenDialog(false);
      };

    if (redirect) {
        return (
            <Redirect to={"/UserSummary"} />
        )
    }

    return (
        <Fade>
            <div>
                <Container fluid>
                    <Row style={{ marginTop: 50 }}>
                        <Col>
                            <h1 className="display-4">
                                <NumberFormat value={totalItems} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                            </h1>
                            <p className="h3 display-4" style={{ color: 'grey', fontSize: '1.9em' }}>Residuos ingresados</p>
                        </Col>
                    </Row>
                    <Row style={{ marginTop: 50 }}>
                        <Col>
                            <Card style={{ alignItems: 'center', borderWidth: 0 }}>
                                <CardImg top src={bottleContainer} style={{ width: 100 }} />
                                <CardBody className="mt-0 pt-2">
                                    <CardTitle className="mb-0 mt-0">
                                        <p className="h3 display-4" style={{ color: 'grey', fontSize: '1.9em', fontWeight: '400' }}>
                                            <NumberFormat value={dataArray[0]} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                        </p>
                                    </CardTitle>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col>
                            <Card style={{ alignItems: 'center', borderWidth: 0 }}>
                                <CardImg top src={canContainer} style={{ width: 100 }} />
                                <CardBody className="mt-0 pt-2">
                                    <CardTitle className="mb-0 mt-0">
                                        <p className="h3 display-4" style={{ color: 'grey', fontSize: '1.9em', fontWeight: '400' }}>
                                            <NumberFormat value={dataArray[1]} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                        </p>
                                    </CardTitle>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col>
                            <Card style={{ alignItems: 'center', borderWidth: 0 }}>
                                <CardImg top src={capContainer} style={{ width: 100 }} />
                                <CardTitle className="mb-0 mt-0">
                                    <p className="h3 display-4" style={{ color: 'grey', fontSize: '1.9em', fontWeight: '400' }}>
                                        <NumberFormat value={dataArray[2]} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                    </p>
                                </CardTitle>
                            </Card>
                        </Col>
                        <Col>
                            <Card style={{ alignItems: 'center', borderWidth: 0 }}>
                                <CardImg top src={batteryContainer} style={{ width: 100 }} />
                                <CardTitle className="mb-0 mt-0">
                                    <p className="h3 display-4" style={{ color: 'grey', fontSize: '1.9em', fontWeight: '400' }}>
                                        <NumberFormat value={dataArray[3]} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                    </p>
                                </CardTitle>
                            </Card>
                        </Col>
                        <Col>
                            <Card style={{ alignItems: 'center', borderWidth: 0 }}>
                                <CardImg top src={phoneContainer} style={{ width: 100 }} />
                                <CardTitle className="mb-0 mt-0">
                                    <p className="h3 display-4" style={{ color: 'grey', fontSize: '1.9em', fontWeight: '400' }}>
                                        <NumberFormat value={dataArray[4]} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                    </p>
                                </CardTitle>
                            </Card>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Button variant="success" size="lg" onClick={handleClickFinish}>Finalizar</Button>
                        </Col>
                    </Row>
                </Container>
                    <Dialog
                        open={openDialog}
                        onClose={handleCancel}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{" Hola! ¿Ya terminaste de reciclar?"}</DialogTitle>
                        <DialogActions>
                        <Button onClick={handleCancel} color="primary">
                            No todavía
                        </Button>
                        <Button onClick={handleOK} color="primary" autoFocus>
                            Si, ya terminé
                        </Button>
                        </DialogActions>
                    </Dialog>
            </div>
        </Fade>

    );
}

export default UserInput;