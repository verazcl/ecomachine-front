import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router'
import { Fade } from 'reactstrap';
import axios from 'axios';
import background from '../images/pexels-mali-maeder-802221.jpg';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const MagneticStatus = props => {
    const [redirect, setRedirect] = useState(false);
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [redirectUserAction, setRedirectUserAction] = useState(false);
    const [initialData, setInitialData] = useState(null);

    const attempts = 5;
    let attempt = 0;

    useEffect(() => {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();

        const getStatus = async () => {
            try {
                let result = await axios.get(`sensors/magnetic.txt`, { cancelToken: source.token });
                setData(result.data);
            } catch (error) {
                if (axios.isCancel(error)) {
                    console.log("axios cancelled");
                  } else {
                        if (error.response) {
                            setError("Ha ocurrido un error en el servidor");
                        } else if (error.request) {
                            setError("Verifique la conexión a internet");
                        } else {
                            setError("Error al cargar datos");
                        }
                    }
            }
        }
        if (initialData == null) {
            getData();
            setInitialData(data);
        }
        
        const intervalId = setInterval(async () => {
            if(data != initialData) {
                clearInterval(intervalId);
                setRedirectUserAction(true);
            }

            if (attempt++ == attempts) {
                clearInterval(intervalId);
                setRedirect(true);
            }
            await getStatus();
        }, 2000);

        return () => {
            source.cancel();
            clearInterval(intervalId);
        }
    }, [data, initialData, attempt]);

    if (redirectUserAction) {
        localStorage.setItem('initialData', initialData);
        return (
            <Redirect to={
                { pathname: '/UserInput' }
            } />
        ) 
    }

    if (redirect) {
        return (
            <Redirect to={"/HowItWorks"} initialData={initialData} />
        )
    }

    return (
        <Fade>
            <Grid
            container
            spacing={0}
            direction="row"
            alignItems="center"
            justify="center"
            style={{ minHeight: '100vh', height: "100vh", backgroundImage: `url(${background})`, backgroundSize: 'cover' }}
            >
                <Grid item xs={12}>
                    <Typography style={{ textAlign: 'center', color: 'white', fontSize: '2.8em' }}>Se detectó la apertura de una puerta, sistema bloqueado.</Typography>
                </Grid>   
            </Grid>
        </Fade>
    );
}

export default MagneticStatus;