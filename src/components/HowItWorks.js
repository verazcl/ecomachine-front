import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Fade, Card, CardImg, CardBody, CardTitle  } from 'reactstrap';
import firstStep from '../images/icons8-wine-bottle-64.png';
import secondtep from '../images/icons8-qr-code-80.png';
import thirdStep from '../images/icons8-email-80.png';
import fourthStep from '../images/icons8-checkmark-96.png';
import { Redirect } from 'react-router'
import axios from 'axios'

const HowItWorks = props => {
    const [redirect, setRedirect] = useState(false);
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [redirectUserAction, setRedirectUserAction] = useState(false);
    const [initialData, setInitialData] = useState(null);

    const attempts = 5;
    let attempt = 0;

    useEffect(() => {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();

        const getData = async () => {
            try {
                let result = await axios.get(`sensors/cont.txt`, { cancelToken: source.token });
                setData(result.data);
            } catch (error) {
                if (axios.isCancel(error)) {
                    console.log("axios cancelled");
                  } else {
                        if (error.response) {
                            setError("Ha ocurrido un error en el servidor");
                        } else if (error.request) {
                            setError("Verifique la conexión a internet");
                        } else {
                            setError("Error al cargar datos");
                        }
                    }
            }
        }
        if (initialData == null) {
            getData();
            setInitialData(data);
        }
        
        const intervalId = setInterval(async () => {
            if(data != initialData) {
                clearInterval(intervalId);
                setRedirectUserAction(true);
            }

            if (attempt++ == attempts) {
                clearInterval(intervalId);
                setRedirect(true);
            }
            await getData();
        }, 2000);

        return () => {
            source.cancel();
            clearInterval(intervalId);
        }
    }, [data, initialData, attempt]);

    if (redirectUserAction) {
        localStorage.setItem('initialData', initialData);
        return (
            <Redirect to={
                { pathname: '/UserInput' }
            } />
        ) 
    }

    if (redirect) {
        return (
            <Redirect to={"/VideoInfo"} initialData={initialData} />
        )
    }


        return (
        <Fade>
            <div>
                    <Container fluid>
                        <Row style={{marginTop: 20}}>
                            <Col><h1 className="display-4">¿Cómo funciona?</h1></Col>
                        </Row>
                        <Row style={{marginTop: 30}}>
                            <Col>
                                <Card style={{alignItems: 'center', borderWidth: 0}}>
                                    <CardImg top src={firstStep} style={{width: 100}} />
                                    <CardBody>
                                        <CardTitle>
                                            <p className="h3 display-4" style={{color : 'gray', fontSize: '1.9em'}}>
                                                Ingresa un material<br />para reciclar
                                            </p>
                                        </CardTitle>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col>
                                <Card style={{alignItems: 'center', borderWidth: 0}}>
                                <CardImg top src={secondtep} style={{width: 100}} />
                                    <CardBody>
                                        <CardTitle><p className="h3 display-4" style={{color : 'gray', fontSize: '1.9em'}}>Escanea el< br/>código QR</p></CardTitle>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Card style={{alignItems: 'center', borderWidth: 0}}>
                                <CardImg top src={thirdStep} style={{width: 100}} />
                                    <CardBody>
                                        <CardTitle><p className="h3 display-4" style={{color : 'gray', fontSize: '1.9em'}}>Registrate con un<br />correo electrónico</p></CardTitle>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col>
                                <Card style={{alignItems: 'center', borderWidth: 0}}>
                                <CardImg top src={fourthStep} style={{width: 100}} />
                                    <CardBody>
                                        <CardTitle><p className="h3 display-4" style={{color : 'gray', fontSize: '1.9em'}}>Canjea tus puntos<br />en ECOClub.cl</p></CardTitle>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </div>
        </Fade>
        );
    }
 
export default HowItWorks;