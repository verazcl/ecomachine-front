import React from 'react';
 
import { NavLink } from 'react-router-dom';
 
const Navigation = () => {
    return (
       <div>
          <NavLink to="/">Welcome</NavLink>
          <NavLink to="/Ranking">Ranking</NavLink>
       </div>
    );
}
 
export default Navigation;