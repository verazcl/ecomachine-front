import React, { Component, useEffect, useState } from 'react';
import axios from 'axios';
import { Container, Row, Col, Fade, Spinner, Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button  } from 'reactstrap';
import bottleContainer from '../images/icons8-wine-bottle-100.png';
import canContainer from '../images/icons8-beer-can-100.png';
import capContainer from '../images/icons8-bottle-cap-100.png';
import batteryContainer from '../images/icons8-battery-100.png';
import phoneContainer from '../images/icons8-iphone-x-64.png';
import { Redirect } from 'react-router'
import NumberFormat from 'react-number-format';

const TotalStats = () => {
    const [redirect, setRedirect] = useState(false);
    const [data, setData] = useState(null);
    const [statsData, setStatsData] = useState([]);
    const [error, setError] = useState(null);
    const [redirectUserAction, setRedirectUserAction] = useState(false);
    const [initialData, setInitialData] = useState(null);

    const attempts = 5;
    let attempt = 0;

    useEffect(() => {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();

        const getData = async () => {
            try {
                let result = await axios.get(`sensors/cont.txt`, { cancelToken: source.token });
                setData(result.data);
            } catch (error) {
                if (axios.isCancel(error)) {
                    console.log("axios cancelled");
                  } else {
                        if (error.response) {
                            setError("Ha ocurrido un error en el servidor");
                        } else if (error.request) {
                            setError("Verifique la conexión a internet");
                        } else {
                            setError("Error al cargar datos");
                        }
                    }
            }
        }

        const getStatsData = async () => {
            try {
                let API_URL = localStorage.getItem('API_URL');
                let MACHINE_CODE = localStorage.getItem('MACHINE_CODE');
                let API_TOKEN = localStorage.getItem('API_TOKEN');
                let result = await axios.post(`${API_URL}/machine/totalStats`, { code: MACHINE_CODE, cancelToken: source.token }, {
                    headers: { 
                        'x-api-key': `${API_TOKEN}`,
                        'Content-Type': 'application/json',
                        'cache-control': 'no-cache'
                    }
                });
                setStatsData(result.data);
            } catch (error) {
                if (axios.isCancel(error)) {
                    console.log("axios cancelled");
                  } else {
                        if (error.response) {
                            setError("Ha ocurrido un error en el servidor");
                        } else if (error.request) {
                            setError("Verifique la conexión a internet");
                        } else {
                            setError("Error al cargar datos");
                        }
                    }
            }
        }

        getStatsData();

        if (initialData == null) {
            getData();
            setInitialData(data);
        }
        
        const intervalId = setInterval(async () => {
            if(data != initialData) {
                clearInterval(intervalId);
                setRedirectUserAction(true);
            }

            if (attempt++ == attempts) {
                clearInterval(intervalId);
                setRedirect(true);
            }
            await getData();
        }, 2000);

        return () => {
            source.cancel();
            clearInterval(intervalId);
        }
    }, [data, initialData, attempt]);

    if (redirectUserAction) {
        localStorage.setItem('initialData', initialData);
        return (
            <Redirect to={
                { pathname: '/UserInput' }
            } />
        ) 
    }

    if (redirect) {
        return (
            <Redirect to={"/Ranking"} initialData={initialData} />
        )
    }
                return (
            <Fade>
                <div>
                <Container fluid>
                        <Row style={{marginTop: 50}}>
                            <Col>
                                <h1 className="display-4">
                                    +<NumberFormat value={statsData.total} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                </h1>
                                <p className="h3 display-4" style={{color : 'grey', fontSize: '1.9em'}}>Residuos reciclados<br />en toda nuestra Red</p>
                            </Col>
                        </Row>
                        <Row style={{marginTop: 50}}>
                            <Col>
                                <Card style={{alignItems: 'center', borderWidth: 0}}>
                                <CardImg top src={bottleContainer} style={{width: 100}} />
                                    <CardBody className="mt-0 pt-2">
                                        <CardTitle className="mb-0 mt-0">
                                            <p className="h3 display-4" style={{color : 'grey', fontSize: '1.9em', fontWeight: '400'}}>
                                                <NumberFormat value={statsData.bottles} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                            </p>
                                        </CardTitle>
                                    </CardBody>
                                </Card>
                            </Col>  
                            <Col>
                                <Card style={{alignItems: 'center', borderWidth: 0}}>
                                <CardImg top src={canContainer} style={{width: 100}} />
                                    <CardBody className="mt-0 pt-2">
                                        <CardTitle className="mb-0 mt-0">
                                            <p className="h3 display-4" style={{color : 'grey', fontSize: '1.9em', fontWeight: '400'}}>
                                                <NumberFormat value={statsData.cans} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                            </p>
                                        </CardTitle>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col>
                                <Card style={{alignItems: 'center', borderWidth: 0}}>
                                <CardImg top src={capContainer} style={{width: 100}} />
                                        <CardTitle className="mb-0 mt-0">
                                            <p className="h3 display-4" style={{color : 'grey', fontSize: '1.9em', fontWeight: '400'}}>
                                                <NumberFormat value={statsData.caps} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                            </p>
                                        </CardTitle>
                                </Card>
                            </Col>
                            <Col>
                                <Card style={{alignItems: 'center', borderWidth: 0}}>
                                <CardImg top src={batteryContainer} style={{width: 100}} />
                                        <CardTitle className="mb-0 mt-0">
                                            <p className="h3 display-4" style={{color : 'grey', fontSize: '1.9em', fontWeight: '400'}}>
                                                <NumberFormat value={statsData.batteries} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                            </p>
                                        </CardTitle>
                                </Card>
                            </Col>
                            <Col>
                                <Card style={{alignItems: 'center', borderWidth: 0}}>
                                <CardImg top src={phoneContainer} style={{width: 100}} />
                                <CardTitle className="mb-0 mt-0">
                                            <p className="h3 display-4" style={{color : 'grey', fontSize: '1.9em', fontWeight: '400'}}>
                                                <NumberFormat value={statsData.phones} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} prefix={''} />
                                            </p>
                                        </CardTitle>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </Fade>
            );
            
    
    
}
 
export default TotalStats;