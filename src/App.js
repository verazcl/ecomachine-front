import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import { makeStyles, useTheme } from '@material-ui/core/styles';
// Componentes
import Welcome from './components/Welcome';
import HowItWorks from './components/HowItWorks';
import VideoInfo from './components/VideoInfo';
import LocalStats from './components/LocalStats';
import TotalStats from './components/TotalStats';
import Ranking from './components/Ranking';
import UserInput from './components/UserInput';
import UserSummary from './components/UserSummary';

const useStyles = makeStyles({
    root: {
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
        padding: 0,
    }
});

function App() {
    const theme = useTheme();
    const classes = useStyles();
    const [state, setState] = React.useState({
      top: false,
      left: false,
      bottom: false,
      right: false,
    });
  
    const toggleDrawer = (anchor, open) => (event) => {
      if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
        return;
      }
  
      setState({ ...state, [anchor]: open });
    };

    return (
        <Router>
            <AppBar position="static" style={{ backgroundColor: '#2e2e2e' }}>
                <Toolbar>
                <Typography variant="h6">
                        ECOClub
                </Typography>
                </Toolbar>
            </AppBar>
            <Container maxWidth="xl" disableGutters={false} className={classes.root}>
                <Switch>
                    <Route exact path="/">
                        <Welcome />
                    </Route>
                    <Route path="/HowItWorks">
                        <HowItWorks />
                    </Route>
                    <Route path="/VideoInfo">
                        <VideoInfo />
                    </Route>
                    <Route path="/LocalStats">
                        <LocalStats />
                    </Route>
                    <Route path="/TotalStats">
                        <TotalStats />
                    </Route>
                    <Route path="/Ranking">
                        <Ranking />
                    </Route>
                    <Route path="/UserInput">
                        <UserInput />
                    </Route>
                    <Route path="/UserSummary">
                        <UserSummary />
                    </Route>
                    <Route component={Error}/>
                </Switch>
            </Container>
        </Router >
    );
}

export default App;